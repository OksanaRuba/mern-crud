# CRUD App

## Overview

**CRUD** app written on **MERN** Stack(  **MongoDB** **Express**, **React** and **Node.js**).

Each user is able to create an account and log in.
Each user has name, email, password. Each user can have admin rights or not. Each user is able to create 1 or more profiles.

A profile has fields: name, gender, birthdate, city. Each user can log out whenever he wants and the session wil not be interrupted if the user logs in at least
once a month.

User (without admin rights) can view only his own profiles (list and details), edit, delete. User (with admin rights) can view his own profiles, other users and their
profiles (separate page), edit / delete users and / or their profiles, give to another user admin rights, see a dashboard page with analytics(how many users are
currently in the system, how many profiles are currently in total, how many profiles are over 18 years old).

## Technical details

#### Backend:

**Node JS**,
**Express**,
Rest API

#### Frontend:

**React JS**,
**Redux**,
UI - **react-bootstrap** components with **Lux** free theme from https://bootswatch.com/,
Single page application (Web)

#### DB models

#### Validation

Validation of forms provided by both **frondend**(**client/helpers/validateInfo.js**) and **backend** sides(**app/middleware/validateProfileForm.js**,
**app/middleware/validateUserForm.js**,
**app/middleware/validateUserFormToUpdate.js**, **app/middleware/validateSignIn.js**).

#### Tokens

Tokens are made with **JWT** lib that creates a new one on the backend side each time user authenticates to a system. Token is stored in localStorage, and then
initially is stored in Redux store.

#### Protected routes

Routes are made proteced with middleware that verifies **JWT** token sent in Header of request(**app/middleware/verifyToken.js**);

#### Password encryption

Passwords in db are stored in encrypted form(**bcrypt** lib hashSync() function). When user authenticates bcrypt compareSync() function checks if passwords match.

#### Stay login in implementation

When user authenticates, user data and user token are stored in localStorage. When page reloads or user closes the window, data from localStorage is stored in
**Redux store**.

#### Tests

Unit tests for frontend side written with **Enzyme** to test for DashboardCard.

#### Screen
Sign in
![Знімок_екрана_2021-08-04_sign_in](/uploads/d379e0b83f2956a2c638a387d61cd05b/Знімок_екрана_2021-08-04_sign_in.png)

Sign up
![Знімок_екрана_2021-08-04_sign_up](/uploads/746f63b96e3fdc5977a904ae1777e79e/Знімок_екрана_2021-08-04_sign_up.png)

Create New Profile
![Знімок_екрана_2021-08-04_create_profile](/uploads/c9bc15a02bfc0c5e49bc196e8c5b75ae/Знімок_екрана_2021-08-04_create_profile.png)

Profiles for admin
![Знімок_екрана_2021-08-04_admin_profiles](/uploads/1da8c65a4d1cac24e4e78fccb63f5fac/Знімок_екрана_2021-08-04_admin_profiles.png)

Dashboard Screen for admin
![Знімок_екрана_2021-08-04_dashboard_admin](/uploads/a9f64b7a9276cf3b0801f2c4f916fedf/Знімок_екрана_2021-08-04_dashboard_admin.png)

Delete Confirmation Modal
![Знімок_екрана_2021-08-04_delete_model](/uploads/515b043a00381850165772eb80fdac73/Знімок_екрана_2021-08-04_delete_model.png)

Edit An Existing Profile
![Знімок_екрана_2021-08-04_update_info_user_by_admin](/uploads/523d0ba8bcc22004805f487d4b8e13db/Знімок_екрана_2021-08-04_update_info_user_by_admin.png)

Create profile for user (for user without admin rights)
![Знімок_екрана_2021-08-04_user_create_profile](/uploads/2a6d8f3effc9e59588e19f42fb23ce10/Знімок_екрана_2021-08-04_user_create_profile.png)


