const { Profile } = require('../models/Profile');

const create = (req, res) => {
  if (!req.body.name || !req.body.gender || !req.body.birthdate || !req.body.city) {
    res.status(400).send({
      message: "Please fill in all the fields!",

    });
    return;
  }

  const profile = {
    name: req.body.name,
    gender: req.body.gender,
    birthdate: req.body.birthdate,
    city: req.body.city,
    userId: req.body.userId
  };

  Profile.create(profile)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the profile."
      });
    });
};

const findAllByUserId = (req, res) => {
  console.log("req.params.userId", req.params.userId)
  Profile.find({ userId: req.params.userId })
    .then(data => {
      console.log('data', data)
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving profiles."
      });
    });
};

const findAllProfiles = (req, res) => {
  Profile.find()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving profiles."
      });
    });
};

const findOne = (req, res) => {
  const id = req.params.id;

  Profile.find({ _id: id })
    .then(data => {
      if (!data) {
        throw new Error();
      }
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving profile with id=" + id
      });
    });
};

const update = (req, res) => {
  const id = req.params.id;

  Profile.updateOne({ _id: id }, req.body)
    .then(data => {
      if (data.n === 1) {
        res.send({
          message: "Profile was updated successfully!"
        });
      } else {
        res.send({
          message: `Cannot update profile`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating profile"
      });
    });
};

const deleteProfile = (req, res) => {
  const id = req.params.id;

  Profile.deleteOne({ _id: id })
    .then(data => {
      if (data.n === 1) {
        res.send({
          message: "Profile was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete profile!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete profile"
      });
    });
};

module.exports = {
  create,
  findAllByUserId,
  findAllProfiles,
  findOne,
  update,
  deleteProfile
};
