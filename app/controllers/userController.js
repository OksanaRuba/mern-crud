const bcrypt = require('bcryptjs');
const { User, validPassword } = require('../models/User');
const { generateToken } = require('../utils/generateToken.js');

const registerUser = (req, res) => {
  if (!req.body.username || !req.body.email || !req.body.password) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  const salt = bcrypt.genSaltSync();
  const passwordEncrypted = bcrypt.hashSync(req.body.password, salt);

  const user = {
    username: req.body.username,
    email: req.body.email,
    password: passwordEncrypted,
    isAdmin: req.body.isAdmin ? req.body.isAdmin : false,
    userId: req.body.userId
  };

  const token = generateToken(req.body.email);

  User.create(user)
    .then(data => {
      res.send({ data, token });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the User."
      });
    });
};

const findAllUsers = (req, res) => {
  User.find()
    .then(data => {
      console.log('data', data)
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Users."
      });
    });
};

const authUser = (req, res) => {
  const { email, password } = req.body;

  User.findOne({
    email
  })
    .then(user => {
      if (!user) {
        throw Error('Invalid email or password');
      }
      if (validPassword(password, user.password)) {
        return user;
      }
      else {
        throw Error('Invalid email or password');
      }
    })
    .then(data => {
      res.send({
        data,
        token: generateToken(data.email)
      })
    })
    .catch(err => {
      let msg = err ? err.message : "Error retrieving User";
      res.status(500).send({
        message: msg
      });
    });
};

const update = (req, res) => {
  const id = req.params.id;
console.log('req.body' , req.body)
  User.updateOne({ _id: id }, req.body)
    .then(data => {
      if (data.n  === 1) {
        res.send({
          message: "User was updated successfully.",
          result : data.n,
        });
        
      } else {
        res.send({
          message: `Cannot update User. Maybe Tutorial was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating User"
      });
    });
};

const deleteUsers = (req, res) => {
  const id = req.params.id;

  User.deleteOne({ _id: id })
    .then(data => {
      if (data.n === 1) {
        res.send({
          message: "User was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete User. Maybe User was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete User"
      });
    });
};

const findOne = (req, res) => {
  const id = req.params.id;

  User.findOne({ _id: id })
    .then(data => {
      if (!data) {
        throw new Error();
      }
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving user"
      });
    });
};

module.exports = {
  registerUser,
  findAllUsers,
  authUser,
  update,
  deleteUsers,
  findOne
};