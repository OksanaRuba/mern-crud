const validateProfileForm = (req, res, next) => {
    // console.log(req.body)
//trim() создает копию строки, удаляет все на­чальные и конечные пробельные символы, а затем возвращает результат.
    
    if (!req.body.name.trim()) {
        return res.status(400).json({ message:'Username required'});
    }

    if (!/^[A-Za-z]+/.test(req.body.name.trim())) {
        return res.status(400).json({ message:'Enter a valid name'})
    }

    if (!req.body.birthdate) {
        return res.status(400).json({ message:'Date is required'});
    }

    if (!req.body.city) {
        return res.status(400).json({ message:'City is required'});
    }

    next();
}

module.exports = { 
    validateProfileForm
};