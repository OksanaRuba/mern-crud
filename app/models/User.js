const bcrypt = require('bcryptjs');
const { Schema, model } = require('mongoose');

const validPassword = function (password, existedPassword) {
  return bcrypt.compareSync(password, existedPassword);
};

const User = new Schema({
  username: { type: String, required: true },
  email: { type: String, unique: true, required: true },
  password: { type: String, required: true },
  isAdmin: { type: Boolean },
  userId: {type: String}
})

module.exports = {
  User: model('User', User),
  validPassword
};

