const express = require("express");
const mongoose = require("mongoose");
//const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

var corsOptions = {
  origin: "http://localhost:3000"
};

app.use(cors(corsOptions));

app.use(express.json());

app.use(express.urlencoded({ extended: true })); //это метод, встроенный в express для распознавания входящего объекта запроса в виде строк или массивов. 

app.get("/", (req, res) => {
  res.json({ message: "Basic server." });
});

require("./routes/userRoutes")(app);
require("./routes/profileRoutes")(app);

const PORT = process.env.PORT || 8080;

const init = async () => {
  try {
    await mongoose.connect(`mongodb+srv://admin:p40ssword@cluster0.i1mfi.mongodb.net/Cluster0?retryWrites=true&w=majority`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true
    })
    app.listen(PORT, () => {
      console.log(`Server has been started ${PORT}`);
    });
  } catch (err) {
    console.error(err);
  }
}

init();


// const express = require("express");
// const mongoose = require('mongoose');
// const bodyParser = require("body-parser");
// const cors = require("cors");

// const app = express();

// require("./routes/userRoutes")(app);
// require("./routes/profileRoutes")(app);

// var corsOptions = {
//   origin: "http://localhost:3000"
// };

// app.use(cors(corsOptions));

// app.use(bodyParser.json());

// app.use(bodyParser.urlencoded({ extended: true }));


// //app.use(cors(corsOptions));

// // app.use(express.json());

// // app.use(express.urlencoded({ extended: true }));

// app.get("/", (req, res) => {
//   res.json({ message: "Basic server." });
// });


// const PORT = process.env.PORT || 8080;

// const init = async () => {
//   try {
//     await mongoose.connect(`mongodb+srv://admin:p40ssword@cluster0.i1mfi.mongodb.net/Cluster0?retryWrites=true&w=majority`, {
//       useNewUrlParser: true,
//       // useUnifiedTopology: true,
//       // useFindAndModify: false,
//       //useCreateIndex: true
//     })
//     app.listen(PORT, () => {
//       console.log(`Server has been started ${PORT}`);
//     });
//   } catch (err) {
//     console.error(err);
//   }
// }

// init();
