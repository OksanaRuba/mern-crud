import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import { Container } from "react-bootstrap";

import "./App.css";
import "./bootstrap.min.css";

import Header from "./components/Header/Header";
import LoginLogic from "./screens/LoginLogic/LoginLogic";
import RegisterLogic from "./screens/RegisterLogic/RegisterLogic";
import ProfilesLogic from "./screens/ProfilesLogic/ProfilesLogic";
import DashboardLogic from "./screens/DashboardLogic/DashboardLogic";
import UsersLogic from "./screens/UsersLogic/UsersLogic";
import UserLogic from "./screens/UserLogic/UserLogic";
import { useSelector } from "react-redux";


const App = () => {
  const user = useSelector(state=>state.userLogin.userInfo)
  console.log(user)
  if (!user?.data.username) {
    return (
      <Router>
        <Switch>
          <Route path="/login" component={LoginLogic} />
          <Route path="/register" component={RegisterLogic} />
          <Route path="*"><Redirect to ="/login"/> </Route>
        </Switch>
      </Router>
    );
  }
  return (
    <Router>
      <Header />
      <main className="py-3">
        <Container>
          <Switch>
            <Route path="/profiles" component={ProfilesLogic} />
            <Route path="/dashboard" component={DashboardLogic} />
            <Route path="/users/:id" component={UserLogic} />
            <Route path="/users" component={UsersLogic} />
            <Route path="/">
              <Redirect to="profiles" />
            </Route>
          </Switch>
        </Container>
      </main>
    </Router>
  );
};

export default App;
