import axios from 'axios';
import { pathConstant } from '../configPaths/pathConstants';
import { PROFILE_CREATE_FAIL, PROFILE_CREATE_SUCCESS, PROFILE_UPDATE_FAIL, PROFILE_UPDATE_REQUEST, PROFILES_UPDATE_SUCCESS } from "../constants/profileConstants";
import { ALL_PROFILES_LIST_FAIL, ALL_PROFILES_LIST_REQUEST, PROFILES_LIST_FAIL, PROFILES_LIST_SUCCESS, PROFILES_DELETE_MODAL, PROFILE_DELETE_FAIL } from '../constants/profilesListConstants';


export const updateProfile = ({ _id, name, gender, birthdate, city, userId }) => async (dispatch, getState) => {

    try {
        dispatch({
            type: PROFILE_UPDATE_REQUEST
        });

        const { userLogin: { userInfo } } = getState();

        if (!userInfo) {
            throw new Error('Not authorized');
        }

        const config = {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': `${userInfo.token}`
            }
        };

        const { data } = await axios.put(
            `${pathConstant}/api/profiles/${_id}`,
            {
                name,
                gender,
                birthdate,
                city,
                userId
            },
            config
        );

        dispatch({
            type: PROFILES_UPDATE_SUCCESS,
            payload: { _id, name, gender, birthdate, city, userId }
        });
    }
    catch (error) {
        dispatch({
            type: PROFILE_UPDATE_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message
        });
    }
}

export const deleteProfile = (profileId) => async (dispatch, getState) => {
    try {

        const { userLogin: { userInfo } } = getState();

        if (!userInfo) {
            throw new Error('Not authorized');
        }

        const config = {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': `${userInfo.token}`
            }
        };

        const { data } = await axios.delete(
            `${pathConstant}/api/profiles/${profileId}`,
            config
        );
        console.log(data)

        dispatch({
            type: PROFILES_DELETE_MODAL,
            payload: profileId
        });
    }
    catch (error) {
        dispatch({
            type: PROFILE_DELETE_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message
        });
    }
}

export const createProfiles = (profile) => async (dispatch, getState) => {
    try {

        const { userLogin: { userInfo } } = getState();

        if (!userInfo) {
            throw new Error('Not authorized');
        }

        const config = {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': `${userInfo.token}`
            }
        };

        const { data } = await axios.post(
            `${pathConstant}/api/profiles`,
            profile,
            config
        );
        console.log('data: ', data);

        dispatch({
            type: PROFILE_CREATE_SUCCESS,
            payload: data
        });
    }
    catch (error) {
        dispatch({
            type: PROFILE_CREATE_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message
        });
    }
};

export const listProfilesByUserId = (userId) => async (dispatch, getState) => {
    try {
        console.log('all profiles bu id')

        const { userLogin: { userInfo } } = getState();

        if (!userInfo) {
            throw new Error('Not authorized for this page!');
        }

        const config = {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': `${userInfo.token}`
            }
        };

        const { data } = await axios.get(
            `${pathConstant}/api/profiles/${userId}`,
            config
        );
        console.log('data ', data)
        dispatch({
            type: PROFILES_LIST_SUCCESS,
            payload: data
        });
    }
    catch (error) {
        dispatch({
            type: PROFILES_LIST_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message
        });
    }
};

export const allProfilesRequest = () => async (dispatch, getState) => {
    try {

        dispatch({
            type: ALL_PROFILES_LIST_REQUEST,
        });

        const { userLogin: { userInfo } } = getState();

        if (!userInfo) {
            throw new Error('Not authorized');
        }

        const config = {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': `${userInfo.token}`
            }
        };

        const { data } = await axios.get(
            `${pathConstant}/api/profiles`,
            config
        );

        dispatch({
            type: PROFILES_LIST_SUCCESS,
            payload: data
        });
    }
    catch (error) {
        dispatch({
            type: ALL_PROFILES_LIST_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message
        });
    }
};
