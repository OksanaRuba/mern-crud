import axios from 'axios';
import { pathConstant } from '../configPaths/pathConstants';
import { PROFILES_DELETE_MODAL, PROFILE_DELETE_FAIL } from '../constants/profilesListConstants';
import { USERS_LIST_FAIL, USERS_LIST_REQUEST, USERS_LIST_SUCCESS } from '../constants/usersListConstants';
import { USER_LOGIN_FAIL, USER_LOGIN_REQUEST, USER_LOGIN_SUCCESS, USER_LOGOUT, USER_REGISTER_FAIL, USER_REGISTER_REQUEST, USER_REGISTER_SUCCESS, USER_UPDATE_FAIL, USER_UPDATE_REQUEST, USER_UPDATE_SUCCESS } from "../constants/userConstants"

export const login = (values) => async (dispatch) => {
    try {
        dispatch({
            type: USER_LOGIN_REQUEST
        });

        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const { data } = await axios.post(
            `${pathConstant}/api/users/login`,
            values,
            config
        );

        dispatch({
            type: USER_LOGIN_SUCCESS,
            payload: data
        });

        localStorage.setItem('userInfo', JSON.stringify(data));
    }
    catch (error) {
        dispatch({
            type: USER_LOGIN_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message
        });
    }
}

export const logout = () => (dispatch) => {
    localStorage.removeItem('userInfo');
    dispatch({
        type: USER_LOGOUT
    });
}

export const register = (username, email, isAdmin, password) => async (dispatch) => {
    try {
        dispatch({
            type: USER_REGISTER_REQUEST
        });

        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const { data } = await axios.post(
            `${pathConstant}/api/users`,
            { username, email, isAdmin, password },
            config
        );

        dispatch({
            type: USER_REGISTER_SUCCESS,
            payload: data
        });

        dispatch({
            type: USER_LOGIN_SUCCESS,
            payload: data
        });

        localStorage.setItem('userInfo', JSON.stringify(data));
    }
    catch (error) {
        dispatch({
            type: USER_REGISTER_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message
        });
    }
};

export const userUpdate = ({ _id, username, email, isAdmin, userId }) => async (dispatch, getState) => {
    try {
        console.log('userId', userId)
        dispatch({
            type: USER_UPDATE_REQUEST
        });

        const { userLogin: { userInfo } } = getState();

        if (!userInfo) {
            throw new Error('Not authorized');
        }

        const config = {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': `${userInfo.token}`
            }
        };

        const { data } = await axios.put(
            `${pathConstant}/api/users/${_id}`,
            {
                username,
                email,
                isAdmin,
                userId
            },
            config
        );
        console.log('data', data)
        if (data.result === 1) {
            dispatch({
                type: USER_UPDATE_SUCCESS,
                payload: { _id, username, email, isAdmin, userId }
            });
        }
    }
    catch (error) {
        dispatch({
            type: USER_UPDATE_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message
        });
    }
}

export const deleteUser = (userId) => async (dispatch, getState) => {
    try {

        const { userLogin: { userInfo } } = getState();

        if (!userInfo) {
            throw new Error('Not authorized');
        }

        const config = {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': `${userInfo.token}`
            }
        };

        const { data } = await axios.delete(
            `${pathConstant}/api/users/${userId}`,
            config
        );
        console.log(data)
        dispatch({
            type: PROFILES_DELETE_MODAL,
            payload: userId
        });
    }
    catch (error) {
        dispatch({
            type: PROFILE_DELETE_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message
        });
    }
}

export const listUsers = () => async (dispatch, getState) => {
    try {
        dispatch({
            type: USERS_LIST_REQUEST
        });

        const { userLogin: { userInfo } } = getState();

        if (!userInfo) {
            throw new Error('Not authorized for this page!');
        }

        const config = {
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': `${userInfo.token}`
            }
        };

        const { data } = await axios.get(
            `${pathConstant}/api/users`,
            config
        );

        dispatch({
            type: USERS_LIST_SUCCESS,
            payload: data
        });
    }
    catch (error) {
        dispatch({
            type: USERS_LIST_FAIL,
            payload: error.response && error.response.data.message
                ? error.response.data.message
                : error.message
        });
    }
};
