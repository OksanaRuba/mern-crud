import { PROFILES_UPDATE_SUCCESS, PROFILE_UPDATE_REQUEST, PROFILE_UPDATE_FAIL, PROFILE_CREATE_REQUEST } from "../constants/profileConstants";
import { PROFILE_CREATE_FAIL, PROFILES_LIST_FAIL, PROFILES_LIST_REQUEST, PROFILE_CREATE_SUCCESS, PROFILES_DELETE_MODAL, ALL_PROFILES_LIST_REQUEST, ALL_PROFILES_LIST_SUCCESS, ALL_PROFILES_LIST_FAIL, PROFILES_LIST_SUCCESS } from "../constants/profilesListConstants";

export const profileUpdateReducer = (state = {}, action) => {
    switch (action.type) {
        case PROFILE_UPDATE_REQUEST:
            return {
                loading: true
            }
        case PROFILE_UPDATE_FAIL:
            return {
                loading: false,
                error: action.payload
            }
        default:
            return state
    }
};

export const profilesListReducer = (state = { profiles: [] }, { type, payload }) => {
    switch (type) {

        case PROFILE_CREATE_REQUEST:
            return {
                loading: true
            }
        case PROFILE_CREATE_SUCCESS:
            return {
                ...state,
                loading: false,
                profiles: [...state.profiles, payload]
            }
        case PROFILE_CREATE_FAIL:
            return {
                loading: false,
                error: payload
            }
        case ALL_PROFILES_LIST_REQUEST:
            return {
                loading: false,
                profiles: []
            }
        case ALL_PROFILES_LIST_SUCCESS:
            return {
                loading: false,
                profiles: payload,
            }
        case ALL_PROFILES_LIST_FAIL:
            return {
                loading: false,
                error: payload
            }
        case PROFILES_LIST_REQUEST:
            return {
                ...state,
                loading: true,
                profiles: []
            }
        case PROFILES_LIST_SUCCESS:
            return {
                ...state,
                loading: false,
                profiles: payload
            }
        case PROFILES_LIST_FAIL:
            return {
                ...state,
                loading: false,
                error: payload
            }
        case PROFILES_UPDATE_SUCCESS:
            return {
                ...state,
                loading: false,
                profiles: state.profiles.map((profile) => profile._id === payload._id ? payload : profile)
            }
        case PROFILES_DELETE_MODAL:
            return {
                ...state,
                loading: false,
                profiles: state.profiles.filter(profile => profile._id !== payload)
            }
        default:
            return state
    }
};

// export const allProfilesListReducer = (state = { profiles: [] }, action) => {
//     switch (action.type) {
//         case ALL_PROFILES_LIST_REQUEST:
//             return {
//                 loading: true,
//                 profiles: []
//             }
//         case ALL_PROFILES_LIST_SUCCESS:
//             return {
//                 loading: false,
//                 profiles: action.payload
//             }
//         case ALL_PROFILES_LIST_FAIL:
//             return {
//                 loading: false,
//                 error: action.payload
//             }
//         default:
//             return state
//     }
// };

