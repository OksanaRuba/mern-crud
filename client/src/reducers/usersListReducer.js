import { USERS_LIST_FAIL, USERS_LIST_REQUEST, USERS_LIST_SUCCESS } from "../constants/usersListConstants";
import {PROFILE_DELETE_REQUEST, PROFILES_DELETE_MODAL, PROFILE_DELETE_FAIL  } from '../constants/profilesListConstants';
import { USER_LOGIN_FAIL, USER_LOGIN_REQUEST, USER_LOGIN_SUCCESS, USER_LOGOUT, USER_UPDATE_FAIL, USER_UPDATE_REQUEST, USER_UPDATE_SUCCESS } from "../constants/userConstants";

export const userLoginReducer = (state = {},{ type, payload }) => {
    switch (type) {
        case USER_LOGIN_REQUEST:
            return {
                ...state,
                loading: true
            }
        case USER_LOGIN_SUCCESS:
            return {
                ...state,
                loading: false,
                userInfo: payload
            }
        case USER_LOGIN_FAIL:
            return {
                ...state,
                loading: false,
                error: payload
            }
        case USER_LOGOUT: {
            return {}
        }
        default:
            return state
    }
};

export const usersListReducer = (state  = { users: [] }, { type, payload }) => {
    switch (type) {
        case USERS_LIST_REQUEST:
            return {
                ...state,
                loading: true,
                users: []
            }
        case USERS_LIST_SUCCESS:
            return {
                ...state,
                loading: false,
                users: payload
            }
        case USERS_LIST_FAIL:
            return {
                ...state,
                loading: false,
                error: payload
            }
            case USER_UPDATE_REQUEST:
            return {
                ...state,
                loading: true
            }
        case USER_UPDATE_SUCCESS:
            console.log('payload', payload )
            return {
                ...state,
                loading: false,
                users: state.users.map((user) => user._id === payload._id ? payload : user )
            }
        case USER_UPDATE_FAIL:
            return {
                ...state,
                loading: false,
                error: payload
            }
            case PROFILE_DELETE_REQUEST:
                return {
                    ...state,
                    loading: true
                }
            case PROFILES_DELETE_MODAL:
                return {
                    ...state,
                    loading: false,
                    users: state.users.filter(user => user._id !== payload)
                }
            case PROFILE_DELETE_FAIL:
                return {
                    ...state,
                    loading: false,
                    error: payload
                }
        default:
            return state
    }
};
