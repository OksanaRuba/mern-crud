import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Row, Col } from 'react-bootstrap';

import DashboardCard from '../../components/DashboardCard/DashboardCard';
import { listUsers } from '../../actions/userListActions';
import { allProfilesRequest } from '../../actions/profilesListActions';
import { useHistory } from 'react-router';

const DashboardLogic = () => {
    const dispatch = useDispatch();
    const usersList = useSelector(state => state.usersList);
    const profilesList = useSelector(state => state.profilesList);

    const userLogin = useSelector(state => state.userLogin);
    const { userInfo } = userLogin;

    const history = useHistory();

    const countUsers = () => {
        return usersList.users.length;
    };

    const countProfiles = () => {
        return profilesList.profiles.length;
    }

    const convertDateToAge = (date) => {
        const dateObj = new Date(date);
        const ageDifMs = Date.now() - dateObj.getTime();
        const ageDate = new Date(ageDifMs); 
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

    const isAdult = (birthdate) => {
        return convertDateToAge(birthdate) > 18 ? true : false;
    }

    useEffect(() => {
        if (!userInfo.data.isAdmin) {
            history.push('/')
        }
    }, [userInfo.data.isAdmin, history])

    useEffect(() => {
        if (!usersList.users.length) {
            dispatch(listUsers())
        }
    }, [dispatch, usersList.users.length])

    useEffect(() => {
        if (!profilesList.profiles.length) {
            dispatch(allProfilesRequest())
        }
    }, [dispatch, profilesList.profiles.length])

    const dashboardCards = [
        {
            title: 'Users',
            count: countUsers()
        },
        {
            title: 'Profiles',
            count: countProfiles()
        },
        {
            title: 'Profiles over 18 y.o.',
            count: profilesList.profiles.filter(profile => isAdult(profile.birthdate)).length
        }
    ]

    return (
        <React.Fragment>
            <h1>Dashboard: </h1>
            <Row>
                {dashboardCards.map(dashboardCard => (
                    <Col key={dashboardCard.title} sm={12} md={6} lg={4} xl={3}>
                        <DashboardCard 
                            cardInfo={dashboardCard} 
                            />
                    </Col>
                ))}
            </Row>
        </React.Fragment>
    )
};

export default DashboardLogic;
