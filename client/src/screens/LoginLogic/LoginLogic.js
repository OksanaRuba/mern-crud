import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import Message from '../../components/Message/Message';
import Loader from '../../components/Loader/Loader';
import FormContainer from '../../components/FormContainer/FormContainer';
import { login } from '../../actions/userListActions';


const LoginLogic = () => {
    const [values, setValue] = useState({ email: '', password: '' });
    const dispatch = useDispatch();

    const userLogin = useSelector(state => state.userLogin);
    const { loading, error } = userLogin;

    const onChange = ({ target }) => {
        setValue({ ...values, [target.name]: target.value })
    }


    const submitHandler = (e) => {
        e.preventDefault();
        dispatch(login(values));
    }

    return (
        <FormContainer>
            <h1>Sign In</h1>
            {error &&
                (<Message variant='danger'>
                    {error}
                </Message>)}
            {loading && <Loader />}
            <Form onSubmit={submitHandler}>
                <Form.Group controlId='email'>
                    <Form.Label>Email Adress</Form.Label>
                    <Form.Control
                        type='email'
                        name="email"
                        placeholder='Enter email'
                        value={values.email}
                        onChange={onChange}
                    ></Form.Control>
                </Form.Group>
                <Form.Group controlId='password'>
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type='password'
                        name="password"
                        placeholder='Enter password'
                        value={values.password}
                        onChange={onChange}
                    ></Form.Control>
                </Form.Group>

                <Button type='submit' variant='primary'>
                    Sign In
                </Button>
            </Form>

            <Row className='py-3'>
                <Col>
                    New User? <Link to={'/register'}>
                        Register
                    </Link>
                </Col>
            </Row>
        </FormContainer>

    )
};

export default LoginLogic;
