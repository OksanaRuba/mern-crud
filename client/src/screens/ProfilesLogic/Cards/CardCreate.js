import React from 'react';
import { Col, Row, Card, Button } from 'react-bootstrap';

export default function CardCreate({onAdd}) {
    return (
        <Row>
        <Col className='createProfile' >
            <Card>
                <Card.Body>
                    <Card.Title>
                        <strong>Create New Profile</strong>
                    </Card.Title>
                    <Button variant="outline-primary" onClick={onAdd}>Create</Button>
                </Card.Body>
            </Card>
        </Col>
    </Row>
    )
}
