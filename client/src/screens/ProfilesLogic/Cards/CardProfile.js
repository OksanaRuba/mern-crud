import React, { useMemo } from 'react';
import { Card, Button } from 'react-bootstrap';
// import moment from 'moment';

export default function CardProfile({onUpdate, onDelete, profile}) {
  // const date = moment().format('MM/DD/YYYY');
  const formattedDate = useMemo(() => profile.birthdate.slice(0, 10), [profile]);
    return (
        <Card className="my-3 p-3 rounded">
        <Card.Body>
          <Card.Title>
            <strong>{profile.name}</strong>
          </Card.Title>
          <Card.Text>{profile.gender}</Card.Text>
          <Card.Text>{formattedDate}</Card.Text>
          <Card.Text>{profile.city}</Card.Text>
          <Button variant="outline-secondary" onClick={onUpdate}>Edit</Button>
          <Button variant="outline-danger" onClick={onDelete}>Delete</Button>
        </Card.Body>
      </Card>
    )
}
