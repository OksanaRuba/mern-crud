import React from 'react';
import { Button, Modal } from 'react-bootstrap';


export default function ModalDelete({onSubmit, onCancel}) {
    return (
        <Modal show={true}>
            <Modal.Body>
                Are you sure?
            </Modal.Body>
            <Modal.Footer>
            {/* <Button variant="danger" onClick={() => handleDeleteModal(profile.id)}> */}
                <Button variant="danger" onClick={onSubmit}>
                    Yes, delete
                </Button>
               {/* </Modal.Footer> <Button variant="secondary" onClick={handleCloseDeleteConfirmationModal} */}
                <Button variant="secondary" onClick={onCancel}>
                    No, cancel
                </Button>
            </Modal.Footer>
        </Modal>
    );
}
