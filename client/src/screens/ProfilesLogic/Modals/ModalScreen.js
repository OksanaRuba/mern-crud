import { Formik, Form, Field, ErrorMessage } from "formik";
import React from "react";
import { Button, Card, Modal, Row, Col } from "react-bootstrap";
import * as Yup from "yup";

const DEFAULT_PROFILE = {
  name: "",
  gender: "",
  birthdate: "",
  city: "",
};

const validationSchema = Yup.object({
  name: Yup.string()
    .max(15, "Must be 15 characters or less")
    .required("Required"),
  gender: Yup.string().required("Required"),
  birthdate: Yup.date()
    .max(new Date(), "Future date not allowed")
    .required("Required"),
  city: Yup.string().required("Required"),
});

const Error = ({ children }) => {
  return <p className="error">{children}</p>;
};

export default function ModalScreen({ show, onHide, onSubmit, profile }) {
  const isSubmit = (profile) => {
    onSubmit(profile);
    onHide();
  };
  return (
    <Modal show={show} onHide={onHide} className="create-profile">
      <Modal.Body>
        <Card>
          <Card.Body>
            <Card.Title>
              <strong>
                {profile ? "Update Profile" : "Create New Profile"}
              </strong>
            </Card.Title>

            <Formik
              initialValues={profile || DEFAULT_PROFILE}
              validationSchema={validationSchema}
              onSubmit={isSubmit}
            >
              <Form>
                <label htmlFor="name">name</label>
                <Field name="name" type="text" />
                <ErrorMessage name="name" component={Error} />

                <label htmlFor="gender">gender</label>
                <Field name="gender" type="text" />
                <ErrorMessage name="gender" component={Error} />

                <label htmlFor="birthdate">birthdate</label>
                <Field name="birthdate" type="date" />
                <ErrorMessage name="birthdate" component={Error} />

                <label htmlFor="city">city</label>
                <Field name="city" type="city" />
                <ErrorMessage name="city" component={Error} />
                <Row>
                  <Col className="createProfile">
                    <Button type="submit" >
                     Update
                    </Button>
                  </Col>
                </Row>
              </Form>
            </Formik>
          </Card.Body>
        </Card>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={onHide}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
