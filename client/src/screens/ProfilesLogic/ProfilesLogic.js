import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Row, Col } from "react-bootstrap";
import Message from "../../components/Message/Message";
import Loader from "../../components/Loader/Loader";
import { listProfilesByUserId, allProfilesRequest, createProfiles, updateProfile, deleteProfile } from "../../actions/profilesListActions";
import CardCreate from "./Cards/CardCreate";
import ModalScreen from "./Modals/ModalScreen";
import CardProfile from "./Cards/CardProfile";
import ModalDelete from "./Modals/ModalDelete";

import "./profiles.css";

const ProfilesLogic = ({ userId }) => {
  const [showCreateModal, setShowCreateModal] = useState(false);
  const dispatch = useDispatch();
  const { profilesList, userLogin } = useSelector((state) => state);
  const [deleteModalItem, setDeleteModalItem] = useState(-1);
  const [isOnDeleteModal, setIsOnDeleteModal] = useState(false)

  const [selectedUser, setSelectedUser] = useState({
    name: "",
    city: "",
    gender: "",
    birthdate: "",
  });

  const onDeleteModal = (id) => {
    setDeleteModalItem(id);
    toggleDeleteModal();
  }

  const onCancelDelete = () => {
    setDeleteModalItem(-1);
    toggleDeleteModal();
  }

  const submitDelete = () => {
    toggleDeleteModal();
    dispatch(deleteProfile(deleteModalItem));
    setDeleteModalItem(-1);
  }

  const toggleDeleteModal = () => setIsOnDeleteModal((state) => !state)

  const { loading, error, profiles } = profilesList;
  const { userInfo } = userLogin;

  const handleShowCreateModal = (profile) => {
    setSelectedUser(profile);
    setShowCreateModal(true);
  };

  const handleCloseCreateModal = () => {
    setShowCreateModal(false);
  };

  const onSubmit = (profile) => {
    if (profile._id) {
      dispatch(updateProfile(profile));
    } else {
      dispatch(createProfiles({ ...profile, userId: userInfo.data._id }));
    }
  };

  useEffect(() => {
    //console.log('user id: ', userId, ' user.info: ', userInfo.data._id)
    userInfo.data.isAdmin
      ? userId
        ? dispatch(listProfilesByUserId(userId))
        : dispatch(allProfilesRequest())
      : dispatch(listProfilesByUserId(userInfo.data._id))

  }, [dispatch, userId, userInfo]);

  return (
    <React.Fragment>
      {
        <ModalScreen
          show={showCreateModal}
          onSubmit={onSubmit}
          onHide={handleCloseCreateModal}
          profile={selectedUser}
        />
      }
      <h1>Profiles: </h1>
      {loading ? (
        <Loader />
      ) : error ? (
        <Message variant="danger">{error}</Message>
      ) : (
        <Row>
          {profiles.map((profile) => (
            <Col
              key={profile._id}
              sm={6}
              md={3}
              lg={4}
              xl={3}
              className="create-cards"
            >
              <CardProfile
                onUpdate={() => {
                  handleShowCreateModal(profile);
                }}
                // onDelete={onevent}
                onDelete={() => onDeleteModal(profile._id)}
                profile={profile}
              />
            </Col>
          ))}
          <Col sm={6} md={2} lg={4} xl={3} className="create-card">
            <CardCreate onAdd={() => handleShowCreateModal()} />
          </Col>
        </Row>
      )}
      {isOnDeleteModal && <ModalDelete onSubmit={submitDelete} onCancel={onCancelDelete} />}
    </React.Fragment>
  );
};

export default ProfilesLogic;
