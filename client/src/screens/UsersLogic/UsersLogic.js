import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Row, Col } from 'react-bootstrap';

import User from '../../components/user/user';
import Message from '../../components/Message/Message';
import Loader from '../../components/Loader/Loader';
import { listUsers } from '../../actions/userListActions';
import { useHistory } from 'react-router';
import { allProfilesRequest } from '../../actions/profilesListActions';


const UsersLogic = () => {
    const dispatch = useDispatch();
    const history = useHistory();

    const usersList = useSelector(state => state.usersList);
    const { loading, error, users } = usersList;

    const userLogin = useSelector(state => state.userLogin);
    const { userInfo } = userLogin;

    const profilesList = useSelector(state => state.profilesList);
    const { profiles } = profilesList; 

    useEffect(() => {
        if (!userInfo.data.isAdmin) {
            history.push('/');
        } else {
            dispatch(listUsers())
        }
    }, [userInfo.data.isAdmin, history, dispatch]);

    useEffect(() => {
        if (!userInfo) {
            history.push('/login');
        } else {
            dispatch(listUsers())
        }
    }, [userInfo, history, dispatch]);

    useEffect(() => {
        if (!profilesList.profiles.length) {
            dispatch(allProfilesRequest())
        } else {
            dispatch(listUsers())
        }
    }, [profilesList.profiles.length, dispatch])

    return (
        <React.Fragment>
            <h1>Users: </h1>
            {loading ? (<Loader />) : error ? (<Message variant="danger">{error}</Message>) : (<Row>
                {users.filter(user => user._id !== userInfo.data._id).map(user => (
                    <Col key={user._id} sm={12} md={6} lg={4} xl={3}>
                        <User user={user} profilesCount={profiles.filter(profile => profile.userId === user._id).length} />
                    </Col>
                ))}
            </Row>)
            }
        </React.Fragment>
    )
};

export default UsersLogic;
