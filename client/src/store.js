import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { userLoginReducer } from './reducers/usersListReducer';
import { usersListReducer } from './reducers/usersListReducer';
import { profilesListReducer } from './reducers/profilesListReducer';


const reducer = combineReducers({
    userLogin: userLoginReducer,
    usersList: usersListReducer,
    profilesList: profilesListReducer
});

const userInfoFromStorage = JSON.parse(localStorage.getItem('userInfo'));

const initialState = { //объект, представляющий начальное состояние хранилища
    userLogin: { userInfo: userInfoFromStorage }
};

const middleware = [thunk];

const store = createStore(reducer, initialState, composeWithDevTools
    (applyMiddleware(...middleware)));

export default store;
